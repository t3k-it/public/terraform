PROJECT ?= training

.PHONY: build
build: html/index.html
	@:

html/index.html: content.md
	pandoc --standalone -t revealjs -o html/index.html content.md

.PHONY: deploy
deploy: $(PROJECT).pdf build public
	cp -R html/* public/
	cp $(PROJECT).pdf public/

.PHONY: book
book: $(PROJECT).pdf
	@:

$(PROJECT).pdf: content.md
	pandoc --pdf-engine=xelatex -o $(PROJECT).pdf content.md

public:
	mkdir -p public
