---
author: Paweł Tomak
title: 'Terraform 0.12+ training'
revealjs-url: 'reveal.js'
theme: solarized
---

## About me

Paweł Tomak

<pawel@tomak.eu>

https://pawel.tomak.eu

## Presentation

Available at: <https://t3k-it.gitlab.io/public/terraform>

Examples: <https://gitlab.com/t3k-it/public/terraform>

# Agenda

## Basics

* IaC
* Usage / CLI
* State file
* Backends
* Providers
* Syntax
* Building blocks

## Advanced usage

* Functions
* Modules
* count
* for_each
* dynamic Blocks
* state file once again

# Infrastructure as Code

## Why

## When

## Pros

* Time Travel
* Version Control
* Collaboration
* Backup

## Cons

* Longer time before initial working infrastructure

# Usage

## terraform init

## terraform plan

## terraform apply

## terraform destroy

# State file

## What's this?

* it's just a JSON file

## Backends

* local (default)
* S3
* azure
* gcs
* consul

Full list at [Terraform Docs](https://www.terraform.io/docs/backends/types/index.html)

## Lock

* Prevents from simultanous writes to the state file
* Requires support in the backend

# Advanced features

## Functions

Manipulate data, reduce number of repetitive operations to provide configuration.

## Functions worth mentioning

* [cidrsubnet](https://www.terraform.io/docs/configuration/functions/cidrsubnet.html)
* [flatten](https://www.terraform.io/docs/configuration/functions/flatten.html)
* [lookup](https://www.terraform.io/docs/configuration/functions/lookup.html)
* [setproduct](https://www.terraform.io/docs/configuration/functions/setproduct.html)
* [toset](https://www.terraform.io/docs/configuration/functions/toset.html)
* [formatlist](https://www.terraform.io/docs/configuration/functions/formatlist.html)

## Modules

* encapsulate resources in an easy-to-set, easy-to-reuse package.
* are structured the same as our project root directory
* should take input and should also provide output
* can have a providers configuration, although this should be provided from upstream
* can be nested, this might be a bit problematic with local modules (source path)

## Structure

```
>_ tree
.
├── data.tf
├── main.tf
├── outputs.tf
├── README.md
└── variables.tf
```

## Module sources

* [Terraform module registry](https://registry.terraform.io/)
* GitHub links
* local paths (for relative path it needs to start with `./`)

## Count

## Acts as a for loop

```java
locals { a = ["a", "b", "c"] }
resource "local_file" "files" {
  count = lenght(local.a)
  content  = local.a[count.index]
  filename = format("%s.txt", local.a[count.index])
}
```

## Acts as a if statement

```java
locals { with_dead_letter_queue = false }
resource "aws_sqs_queue" "deadletter" {
  count = local.with_dead_letter_queue ? 1 : 0
  ...
}
```

## Accessing resouces with count

```java
provider "local" {}
locals { a = ["a", "b", "c"] }
resource "local_file" "files" {
  count = lenght(local.a)
  content  = local.a[count.index]
  filename = format("%s.txt", local.a[count.index])
}
output "afile" { value = local_file.files[0].filename }
output "files" { value = local_file.files }
output "filenames" { value = local_file.files.*.filename }
```

# For Each

## It's like count, but better

```java
locals { a = ["a", "b", "c"] }
resource "local_file" "files" {
  for_each = toset(local.a)
  content  = each.value
  filename = format("%s.txt", each.value)
}
```

## Accessing resources with for_each

```java
provider "local" {}
locals { a = ["a", "b", "c"] }
resource "local_file" "files" {
  for_each = toset(local.a)
  content  = each.value
  filename = format("%s.txt", each.value)
}
output "afile" { value = local_file.files["a"].filename }
output "files" { value = local_file.files }
output "filenames" { value = [for file in local_file.files : file.filename] }
```

## Advantages of for_each over count

* creates iterator
* more readable (short name iterator vs long array access using counter.index)
* access resources via their key, not numerical index
* accepts iterable types (maps, lists, sets)
* resources are in hash map rather than array

## Practice

Go to [exercise/count_vs_for_each](https://gitlab.com/t3k-it/public/terraform/-/tree/master/exercise/count_vs_for_each) and follow the steps from the README.md

# Dynamic blocks

## Syntax

```java
dynamic "<resources name of the block>" {
  for_each = <set or dict variable>
  iterator = "iterator_name"
  content {
    item1 = iterator_name.value["item1"]
    item2 = iterator_name.value["item2"]
  }
}
```

## Example - AWS Security Group

Our input variable
```java
var.ingress_rules = [
  {
    from_port   = "80"
    to_port     = "80"
    cidr_blocks = ["0.0.0.0/0"]
  },
  {
    from_port   = "443"
    to_port     = "443"
    cidr_blocks = ["0.0.0.0/0"]
  }
]
```

## Example - AWS Security Group

```java
resource "aws_security_group" "dynamic" {
  name = "dynamicSecurityGroup"
  vpc_id = var.vpc_id

  dynamic "ingress" {
    for_each = var.ingress_rules
    content {
      from_port   = ingress.value["from_port"]
      to_port     = ingress.value["from_port"]
      protocol    = lookup(ingress.value, "protocol", "-1")
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }
}
```

## Example - AWS Security Group

```java
resource "aws_security_group" "dynamic" { [...]
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
```

## More complex example - with nested blocks

This Terraform registry module
[iam_policy](https://registry.terraform.io/modules/grodzik/iam_policy/aws/)
uses nested dynamic blocks in it's
[data.tf](https://github.com/grodzik/terraform-aws-iam_policy/blob/master/data.tf)
file to produce "aws_iam_policy_document"

## Future features

* try
* can
