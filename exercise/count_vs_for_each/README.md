# count vs for_each

Pupose of this exercise is to observe difference between using `count` to
create multiple resources based on a template and doing the same using
`for_each`.

Let's start with creating all the resources that will be needed to perform this task.

First we need to `init` terraform backend using following command:

    terraform init

This is necessary step to proceed with any terraform command.

Next, let's create all the resources. For the purpose of this exercise we will
just provision couple of files locally.

    terraform plan -out tfplan

After verifying terraform planned actions, let's approve them and actually
create that files:

    terraform apply -auto-aprove tfplan

Now, let's assume that we actually don't need `*_c.txt` files. So we edit
`main.tf` file and remove line 8. - "c".

This time we will perform apply selectivly, to gain visibility in what's going on.
Following command will remove `for_each_c.txt` file:

    terraform apply -target=local_file.foreach_files

To no surprise terraform will show, that it is going to remove `for_each_c.txt`
file. Answer yes and let terraform remove that file.

Now it's time for `count_c.txt`. We will remove it in similar way as previously:

    terraform apply -target=local_file.count_files

This time there's more to analise. Terraform want's to remove two resouces and
create one additional. If we look carefully in the terraform output, we will
notice, that terraform will remove one file, and one will be replaced, but with
force remove and create. This is because `count` to create number of similar
resources keeps them in simple array. `for_each` on the other hand, because we
used `toset` function (works with using a map as well) creates a key-value
mapping to keep track of the resources. This enables terraform to just remove a
resource even from the middle of the set.

Play with this a bit by adding or removing files from the beginning or end of
the list and check each time what terraform will do in both cases - count and
for_each.
