provider "local" {
}

locals {
  files = [
    "a",
    "b",
    "c",
    "d"
  ]
}

resource "local_file" "foreach_files" {
  for_each = toset(local.files)
  content  = each.value
  filename = format("for_each_%s.txt", each.value)
}

resource "local_file" "count_files" {
  count    = length(local.files)
  filename = format("count_%s.txt", local.files[count.index])
  content  = local.files[count.index]
}

output "a" {
  value = local_file.foreach_files["a"].filename
}
